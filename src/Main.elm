module Main exposing (CardType(..), Model, Msg(..), init, main, update, view)

import Browser
import Element as E
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Element.Input as Input
import Html exposing (Html)
import List.Extra
import Maybe.Extra
import Random
import Random.Extra
import Random.List
import Words exposing (allWords)



---- MODEL ----


type Color
    = Blue
    | Red


generateColor : Random.Generator Color
generateColor =
    Random.int 0 1
        |> Random.map
            (\i ->
                if i == 0 then
                    Blue

                else
                    Red
            )


type CardType
    = ColorCard Color
    | Neutral
    | Assasin


type alias BoardCard =
    ( String, CardType, Bool )


type alias BoardState =
    List BoardCard


allWordsFound : Color -> BoardState -> Bool
allWordsFound color activeWords =
    List.filter
        (\( _, cardType, found ) ->
            cardType == ColorCard color && not found
        )
        activeWords
        |> List.length
        |> (==) 0


isAssasin : BoardCard -> Bool
isAssasin ( _, cardType, _ ) =
    cardType == Assasin


generateActiveWords : List String -> Random.Generator (List String)
generateActiveWords words =
    Random.list 25 (Random.Extra.sample words)
        |> Random.map Maybe.Extra.combine
        |> Random.map (Maybe.withDefault [])


generateWordCardTypes : Random.Generator (List CardType)
generateWordCardTypes =
    Random.List.shuffle
        ([ Assasin ] ++ List.repeat 8 Neutral ++ List.repeat 8 (ColorCard Blue) ++ List.repeat 8 (ColorCard Red))


type GameState
    = NotStarted
    | BlueActive BoardState
    | RedActive BoardState
    | BlueWin BoardState
    | RedWin BoardState


createColorGameState : Color -> List String -> List CardType -> ( Color, GameState )
createColorGameState startColor words wordCardTypes =
    case startColor of
        Blue ->
            ( Blue
            , BlueActive
                (List.Extra.zip3
                    words
                    wordCardTypes
                    (List.repeat 25 False)
                )
            )

        Red ->
            ( Red
            , RedActive
                (List.Extra.zip3
                    words
                    wordCardTypes
                    (List.repeat 25 False)
                )
            )


type alias Model =
    { availableWords : List String
    , gameState : GameState
    }


init : ( Model, Cmd Msg )
init =
    ( { availableWords = allWords
      , gameState = NotStarted
      }
    , Cmd.none
    )



---- UPDATE ----


type Msg
    = NoOp
    | StartGame
    | StartGameForPlayerColor ( Color, GameState )
    | WordGuess Color Int
    | AssasinGuess Color Int


swapActiveTeam state =
    case state of
        NotStarted ->
            NotStarted

        BlueActive state_ ->
            RedActive state_

        RedActive state_ ->
            BlueActive state_

        BlueWin state_ ->
            BlueWin state_

        RedWin state_ ->
            RedWin state_


generateInitialState : List String -> Cmd Msg
generateInitialState words =
    let
        startPlayerGen =
            generateColor

        gameStateGen =
            generateActiveWords words

        cardColors =
            generateWordCardTypes
    in
    Random.map3
        createColorGameState
        startPlayerGen
        gameStateGen
        cardColors
        |> Random.generate StartGameForPlayerColor


selectCard : Int -> BoardState -> BoardState
selectCard index =
    List.Extra.updateAt index (\( word_, cardType_, _ ) -> ( word_, cardType_, True ))


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        StartGame ->
            -- @todo
            ( model, generateInitialState model.availableWords )

        StartGameForPlayerColor ( color, gameState ) ->
            ( { model | gameState = gameState }, Cmd.none )

        WordGuess color index ->
            case model.gameState of
                NotStarted ->
                    ( model, Cmd.none )

                BlueWin _ ->
                    ( model, Cmd.none )

                RedWin _ ->
                    ( model, Cmd.none )

                BlueActive activeWords ->
                    List.Extra.getAt index activeWords
                        |> Maybe.map
                            (\( word, cardType, _ ) ->
                                case cardType of
                                    ColorCard Blue ->
                                        ( { model
                                            | gameState =
                                                BlueActive
                                                    (selectCard
                                                        index
                                                        activeWords
                                                    )
                                          }
                                        , Cmd.none
                                        )

                                    ColorCard Red ->
                                        ( { model
                                            | gameState =
                                                RedActive
                                                    (selectCard
                                                        index
                                                        activeWords
                                                    )
                                          }
                                        , Cmd.none
                                        )

                                    Assasin ->
                                        if allWordsFound color activeWords then
                                            ( { model
                                                | gameState =
                                                    BlueWin
                                                        (selectCard
                                                            index
                                                            activeWords
                                                        )
                                              }
                                            , Cmd.none
                                            )

                                        else
                                            ( { model
                                                | gameState =
                                                    RedWin
                                                        (selectCard
                                                            index
                                                            activeWords
                                                        )
                                              }
                                            , Cmd.none
                                            )

                                    Neutral ->
                                        ( { model
                                            | gameState =
                                                RedActive
                                                    (selectCard
                                                        index
                                                        activeWords
                                                    )
                                          }
                                        , Cmd.none
                                        )
                            )
                        |> Maybe.withDefault ( model, Cmd.none )

                RedActive activeWords ->
                    List.Extra.getAt index activeWords
                        |> Maybe.map
                            (\( word, cardType, _ ) ->
                                case cardType of
                                    ColorCard Blue ->
                                        ( { model
                                            | gameState =
                                                RedActive
                                                    (selectCard
                                                        index
                                                        activeWords
                                                    )
                                          }
                                        , Cmd.none
                                        )

                                    ColorCard Red ->
                                        ( { model
                                            | gameState =
                                                BlueActive
                                                    (selectCard
                                                        index
                                                        activeWords
                                                    )
                                          }
                                        , Cmd.none
                                        )

                                    Assasin ->
                                        if allWordsFound color activeWords then
                                            ( { model
                                                | gameState =
                                                    RedWin
                                                        (selectCard
                                                            index
                                                            activeWords
                                                        )
                                              }
                                            , Cmd.none
                                            )

                                        else
                                            ( { model
                                                | gameState =
                                                    BlueWin
                                                        (selectCard
                                                            index
                                                            activeWords
                                                        )
                                              }
                                            , Cmd.none
                                            )

                                    Neutral ->
                                        ( { model
                                            | gameState =
                                                BlueActive
                                                    (selectCard index activeWords)
                                          }
                                        , Cmd.none
                                        )
                            )
                        |> Maybe.withDefault ( model, Cmd.none )

        AssasinGuess color index ->
            ( model, Cmd.none )



---- VIEW ----


viewBoardPlayer : BoardState -> E.Element Msg
viewBoardPlayer board =
    E.none



-- @todo find the right word


viewBoardSpy : BoardState -> E.Element Msg
viewBoardSpy board =
    E.none


viewBoardGrid : Color -> List BoardCard -> E.Element Msg
viewBoardGrid activeColor cards =
    E.wrappedRow [ E.spacing 10 ] (List.indexedMap (viewBoardCard activeColor) cards)


colors =
    { red =
        E.rgb (243.0 / 255.0) 0.0 (44.0 / 255.0)
    , blue =
        E.rgb (21.0 / 255.0) (159.0 / 255.0) (228.0 / 255.0)
    , neutral =
        E.rgb (202.0 / 255.0) (200.0 / 255.0) (185.0 / 255.0)
    , brown =
        E.rgb (240.0 / 255.0) (226.0 / 255.0) (198.0 / 255.0)
    , assasin =
        E.rgb (47.0 / 255.0) (49.0 / 255.0) (63.0 / 255.0)
    }


viewFinishedCard : CardType -> E.Element Msg
viewFinishedCard cardType =
    E.el
        [ Background.color
            (case cardType of
                ColorCard Red ->
                    colors.red

                ColorCard Blue ->
                    colors.blue

                Neutral ->
                    colors.neutral

                Assasin ->
                    colors.assasin
            )
        , E.width (E.px 220)
        , E.height (E.px 136)
        , Border.rounded 5
        , E.padding 10
        ]
        E.none


viewBoardCard : Color -> Int -> BoardCard -> E.Element Msg
viewBoardCard activeColor index ( word, cardType, selected ) =
    if selected then
        viewFinishedCard cardType

    else
        E.el
            [ Background.color colors.brown
            , Border.color colors.brown
            , Border.rounded 5
            , E.pointer
            , E.padding 10
            , Events.onClick (WordGuess activeColor index)
            ]
            (E.column
                [ E.width (E.px 200)
                , E.padding 10
                , Border.color (E.rgba 0.0 0.0 0.0 0.3)
                , Border.solid
                , Border.width 2
                , Border.rounded 5
                ]
                [ E.el
                    [ E.rotate 3.141529
                    , Font.size 16
                    , E.paddingXY 20 5
                    , Border.color (E.rgba 0.0 0.0 0.0 0.3)
                    , Border.widthEach
                        { bottom = 0
                        , left = 0
                        , right = 0
                        , top = 2
                        }
                    ]
                    (E.text word)
                    |> E.el
                        [ E.paddingEach
                            { top = 20
                            , bottom = 5
                            , left = 0
                            , right = 0
                            }
                        ]
                , E.el
                    [ Background.color (E.rgb 1.0 1.0 1.0)
                    , E.width E.fill
                    , E.centerX
                    , E.padding 10
                    , Font.bold
                    ]
                    (E.text word)
                ]
            )


cardWithTextAttributes color =
    [ E.width (E.px 220)
    , E.height (E.px 136)
    , E.centerY
    , E.centerX
    , E.padding 10
    , Font.size 32
    , Background.color color
    , Border.color color
    , Border.rounded 5
    , Font.bold
    , E.pointer
    ]


viewStartButton : E.Element Msg
viewStartButton =
    Input.button
        (cardWithTextAttributes
            colors.neutral
        )
        { label = E.text "Start game"
        , onPress = Just StartGame
        }


view : Model -> Html Msg
view model =
    E.layout [ E.centerX, E.centerY, E.width E.fill, E.height E.fill ]
        (case model.gameState of
            NotStarted ->
                viewStartButton

            BlueActive activeWords ->
                viewBoardGrid Blue activeWords

            RedActive activeWords ->
                viewBoardGrid Red activeWords

            RedWin activeWords ->
                E.column [ E.spacing 20, E.padding 20, E.centerX ]
                    [ E.row [ E.centerX, E.spacing 20 ]
                        [ E.el (cardWithTextAttributes colors.red)
                            (E.text "Red win")
                        , viewStartButton
                        ]
                    , viewBoardGrid Red activeWords
                    ]

            BlueWin activeWords ->
                E.column [ E.spacing 20, E.padding 20, E.centerX ]
                    [ E.row [ E.centerX, E.spacing 20 ]
                        [ E.el (cardWithTextAttributes colors.blue)
                            (E.text "Blue win")
                        , viewStartButton
                        ]
                    , viewBoardGrid Blue activeWords
                    ]
        )



--        (viewBoardCard ( "Helicopter", ColorCard Red, False ))
---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.element
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = always Sub.none
        }
